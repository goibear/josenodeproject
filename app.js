

/*var jose = {
	printfirstName: function(){
		console.log ("my name is JoseLuis");
		console.log (this === jose);
	}
};
	jose.printfirstName();
//the default calling context is global
function doSomethingawesome(){
	console.log ("\n I am Awesome");
	console.log (this === global);

}
doSomethingawesome();

function User(){
	this.name ="";
	this.life =100;
	this.giveLife =function giveLife (targetPlayer){
		targetPlayer.life +=1;
		console.log(this.name + " gave 1 life to " + targetPlayer.name);
	}
}

var JoseLuis = new User();
var Bear = new User();

JoseLuis.name ="JoseLuis";
Bear.name = "Bear";

JoseLuis.giveLife(Bear);
console.log("JoseLuis: " + JoseLuis.life);
console.log("Bear: " + Bear.life);

//adds functions to all objects
User.prototype.uppercut = function uppercut(targetPlayer){
	targetPlayer.life -=3;
	console.log(this.name + " just uppercutted " + targetPlayer.name);
};
Bear.uppercut(JoseLuis);
console.log("JoseLuis: " + JoseLuis.life);
console.log("Bear: " + Bear.life);
//adds properties to all objects

User.prototype.magic = 60;
console.log(JoseLuis.magic);
console.log(Bear.magic);

*/

//this is an example of a module
/*var movies = require('./movies');
movies.printAvatar();
console.log(movies.favMovies);
*/

/*require('./jose');
require('./luis');
*/


//file system mdule.
//var fs = require('fs');
//this creates a text file with the name corn.
//fs.writeFileSync("corn.txt","Corn is good, corn is good fiber");
//reads the file
//console.log(fs.readFileSync("corn.txt").toString());

//this is a path module
//var path = require('path');
//creates a path for index and about 
//var websiteHome = "Desktop/joseluis/index.html";
//var websiteAbout = "Desktop/joseluis/about.html";
//path.normalize takes out any extra forward slashes
//console.log(path.normalize(websiteHome));
//path.dirname gives the directory of website about
//console.log(path.dirname(websiteAbout));
//path .basename gives the name of the file in this case about.html
//console.log(path.basename(websiteAbout));
//path.extname gives you the extension of the file in this case its .html
//console.log(path.extname(websiteAbout));

//prints out beef every two seconds
//setInterval(function(){
//	console.log("beef");
//},2000);
//prints out the directory name
//console.log(__dirname);
//prints out the file name
//console.log(__filename);







//notes for reference 
//== compares values such 19 == '19' which will be true
//=== compares values and types 19 === '19' will be false
//this is a reference to what is calling it or it can be called by a global
//prototyping is used to add additional methods or objects
//fs is known as a file system module when dealing with system modules no include is needed
